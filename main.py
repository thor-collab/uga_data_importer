import itertools
import os

from openpyxl import load_workbook
from pandas import DataFrame
from sys import argv


def add_metadata(json, table):
    metadata_keys = set([key[0] for row in [table_row['metadata'] for table_row in table] for key in row])
    metadata = [x for x in [table_row['metadata'] for table_row in table]]
    return json


def get_chunks(l, n):
    """Yield successive n-sized chunks from l."""
    # @NOTE https://stackoverflow.com/questions/312443/how-do-you-split-a-list-into-evenly-sized-chunks
    for i in range(0, len(l), n):
        yield l[i:i + n]


def find_metadata(iterable, cd_type):
    args = [iter(iterable)] * 3
    metadata = zip(*args)
    return (x for x in metadata if x[1] == cd_type).next()


def process_sheet(workbook, sheet_name):
    sheet = workbook[sheet_name]
    df = DataFrame(sheet.values)

    # Filter out rows that are blank at the first element
    sheet_array = []
    for row in df.itertuples(index=False):
        if row[0] is None or row[1] is None:  # DATA might have stuff at 0
            continue
        else:
            sheet_array.append(row)

    # Restrict the number of elements for each column based on the title row
    num_columns = len(list(filter(None, sheet_array[0])))
    sheet_array = [row[0:num_columns] for row in sheet_array]
    num_rows = len(sheet_array)

    # @NOTE Make assumption that first row = headers, second row = type
    return sheet_array[2:num_rows]


def process_workbook(filename):
    workbook = load_workbook(filename)

    # Read and prep the EXPERIMENT, SUBJECT, ALIQUOT, and DATA sheets
    experiment_tuples = process_sheet(workbook, 'EXPERIMENT')
    subject_tuples = process_sheet(workbook, 'SUBJECT')
    aliquot_tuples = process_sheet(workbook, 'ALIQUOT')
    data_tuples = process_sheet(workbook, 'DATA')

    # Create the output JSONs
    outputs = []
    for experiment in experiment_tuples:
        output_json = {'name': experiment[0]}
        experiment_metadata = find_metadata(experiment[1:], 'EXPERIMENT')
        output_json['summary'] = \
            "Part of %s" % (experiment_metadata[0]) if experiment_metadata[2] is None \
            else "Part of %s: %s" % (experiment_metadata[0], experiment_metadata[2])
        subjects = [{
            'experiment': s[0],
            'id': s[1],
            'metadata': [
                (chunks[1], chunks[0], chunks[2]) for chunks in
                get_chunks(s[2:], 3) if chunks[1] is not None
            ]
        } for s in subject_tuples if s[0] == experiment[0]]
        aliquots = [{
            'experiment': a[0],
            'subject': a[2],
            'id': a[1],
            'dt': a[4],
            'metadata': [
                (chunks[1], chunks[0], chunks[2]) for chunks in
                get_chunks(a[6:], 3) if chunks[1] is not None
            ]
        } for a in aliquot_tuples if a[0] == experiment[0]]
        data = [{
            'experiment': d[0],
            'aliquot': d[1],
            'dt': d[6],
            'values': d[2:6],
            'metadata': [
                (chunks[1], chunks[0], chunks[2]) for chunks in
                get_chunks(d[7:], 3) if chunks[1] is not None
            ]
        } for d in data_tuples if d[0] == experiment[0]]

        # Create the CSV to go along with the json

        outputs.append(output_json)

    return outputs


if __name__ == '__main__':
    # Grab the data folder
    data_directory = argv[1] if len(argv) > 1 else './data'

    # Read each workbook using Openpyxl
    for workbook_file in os.listdir(data_directory):
        if workbook_file.endswith(".xlsx"):
            process_workbook(data_directory + '/' + workbook_file)
