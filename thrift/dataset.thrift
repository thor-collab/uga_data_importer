namespace py uga_data_importer

struct Metadata {
    1: optional string cd_type;
    2: optional string cd_code;
    3: optional string ds_desc;
}

typedef list<Metadata> MetadataList;

typedef string Timestamp;

typedef map<string, string> AmValue;
typedef list<AmValue> AmValues;

struct Dataset {
    1: required string experiment_id;
    2: optional MetadataList experiment_metadata;
    3: required string subject_id;
    4: optional MetadataList subject_metadata;
    5: required string aliquot_id;
    6: required Timestamp aliquot_dt;
    7: optional MetadataList aliquot_metadata;
    8: required Timestamp data_dt;
    9: optional MetadataList data_metadata;
    10: required AmValues data;
}
